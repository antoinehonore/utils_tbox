import subprocess
import numpy as np
import os
import sys
from datetime import datetime
import pickle as pkl
import zlib

date_fmt = "%Y-%m-%d %H:%M:%S"

def compress_obj(obj):
    return zlib.compress(pkl.dumps(obj))

def decompress_obj(obj):
    return pkl.loads(zlib.decompress(obj))

def read_pklz(fname):
    with open(fname,"rb") as fp:
        out = decompress_obj(pkl.load(fp))
    return out


def write_pklz(fname,obj):
    with open(fname,"wb") as fp:
        pkl.dump(compress_obj(obj),fp)


def gdate(date_fmt="%Y-%m-%d %H:%M:%S"):
    return datetime.now().strftime(date_fmt)


def get_freer_gpu():
    """Assuming that torch.cuda.is_available() is True implies nvidia-smi available.
    Tested for
        $ nvidia-smi -h
        NVIDIA System Management Interface -- v450.66
    2022-08-10:
        fixed for
            $ nvidia-smi -h
            NVIDIA System Management Interface -- v510.68.02
    """
    cmd = 'nvidia-smi -q -d Memory | grep -A4 GPU | grep Used'
    out = subprocess.check_output(cmd, shell=True).decode("utf-8")
    memory_used = [int(x.split()[2]) for x in out.split('\n')[:-1]]
    return 'cuda:{}'.format(np.argmin(memory_used))


def pidprint(*arg, flag="status"):
    """
    Behaves like builtin `print` function, but append runtime info before writing to stderr.
    The runtime info is:

    - pid
    - datetime.now()
    - flag, specified as a keyword. if flag =="error" then exit with code 1.
    """
    print("[{}] [{}] [{}]".format(os.getpid(), datetime.now(), flag), " ".join(map(str, arg)), file=sys.stderr)
    if flag=="error":
        sys.exit(1)
    return


def gitloglastcommit():
    """Find the last commit details. Runs `git log -n 1` in subprocess.
    useful for logging info in notebooks"""
    return subprocess.check_output(["git", "log", "-n", "1"]).strip().decode("utf-8")

def githash():
    """Get the current git commit short hash."""
    return subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip().decode("utf-8")
