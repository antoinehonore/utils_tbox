
def pace_to_time(pace_min, pace_sec, tot_km):
    """
    pace_min, float: the "minute" part of the pace
    pace_sec, float: the "second" part of the pace
    tot_km, float: total distance in km."""
    tot_h = (pace_min+pace_sec/60)*tot_km/60;

    tot_min=(tot_h - tot_h//1)*60;
    tot_h=int(tot_h//1)
    print("{:d} h {:d} min".format(tot_h, int(round(tot_min))))
    return tot_h, tot_min